package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            app.approval(new Amount(new BigInteger("2147483648")));
        });
        assertTrue(exception.getMessage().contains("is not in the specified inclusive range"));
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            boolean res = app.approval(new Amount(new BigInteger("-2147483649")));
        });
        assertTrue(exception.getMessage().contains("is not in the specified inclusive range"));
    }

    @Test
    public void LessThan0NeedsApproval() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            boolean res = app.approval(new Amount(new BigInteger("-1")));
        });
        assertTrue(exception.getMessage().contains("is not in the specified inclusive range"));
    }

    @Test
    public void MoreThan1000NeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(new Amount(new BigInteger("1001")));
        assertFalse(res,() -> "Bigger than 1000 needs approval");
    }
}
