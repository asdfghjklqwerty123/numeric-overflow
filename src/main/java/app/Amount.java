package app;

import java.math.BigInteger;

import static org.apache.commons.lang3.Validate.inclusiveBetween;
import static org.apache.commons.lang3.Validate.notNull;

public class Amount {
    private final int value;

    public Amount(BigInteger amount){
        notNull(amount);
        inclusiveBetween(BigInteger.valueOf(0), BigInteger.valueOf(10000), amount);
        this.value = amount.intValue();
    }

    boolean exceedsThreshold(Amount treshold){
        return treshold.value > this.value;
    }
}
